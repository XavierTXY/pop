//
//  Track.swift
//  PoP
//
//  Created by XavierTanXY on 9/3/19.
//  Copyright © 2019 SPH. All rights reserved.
//

import Foundation

class Track {
    
    //Model class track - I assume these are the fields that i need for displaying
    
    private var _id: Int //Not optional
    private var _trackName: String?
    private var _trackImgUrl: String?
    private var _trackSinger: String?
    
    var id: Int {
    
        get {
            return _id
        }
        
        set( newId ) {
            _id = newId
        }
        
    }

    var trackName: String {
        
        get {
            return _trackName!
        }
        
        set( newName ) {
            _trackName = newName
        }
        
        
    }
    
    var trackImgUrl: String {
        
        get {
           return _trackImgUrl!
        }
        
        set( url ) {
            _trackImgUrl = url
        }
    }
    
    var trackSinger: String {
    
        get {
            return _trackSinger!
        }
        
        set( singer ) {
            _trackSinger = singer
        }
        
    }
    
    
    init(trackData: Dictionary<String, AnyObject>) {
        

        if let id = trackData["id"] as? Int {
            self._id = id

        } else {
            self._id = -1
        }
        
        if let trackName = trackData["title"] as? String {
            self._trackName = trackName
            
        } else {
            self._trackName = ""
        }
        
        //Need to deal with nested dictionary to get neccessary data
        if let trackDict = trackData["track_artists"] as? [[String:AnyObject]] {
            if let track_artists = trackDict[0] as? [String:AnyObject] {
            
                if let artistDict = track_artists["artist"] as? [String:AnyObject] {
                    if let name = artistDict["name"] as? String {
                        self._trackSinger = name
                    } else {
                        self._trackSinger = ""
                    }
                    
                    self._trackImgUrl = nil
                    
                    if let imageDict = artistDict["images"] as? [String:AnyObject]  {
                        if let posterDict = imageDict["poster"] as? [String:AnyObject] {
                            if let url = posterDict["url"] as? String {
                                self._trackImgUrl = url
                            } else {
                                self._trackImgUrl = ""
                            }
                        }
                    }
                    
                    
                }
            }
            
        }

    }

 
}
