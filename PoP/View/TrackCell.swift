//
//  TrackCell.swift
//  PoP
//
//  Created by XavierTanXY on 9/3/19.
//  Copyright © 2019 SPH. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

//A class for track cell
class TrackCell: UITableViewCell {

    @IBOutlet weak var trackImgView: UIImageView!
    @IBOutlet weak var trackName: UILabel!
    @IBOutlet weak var trackSinger: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func configureCell(track: Track) {

        self.trackImgView.loadImageUsingCacheWithUrlStringWithoutIndex(url: track.trackImgUrl)
        self.trackName.text = track.trackName
        self.trackSinger.text = track.trackSinger
    }


}
