//
//  ErrorAlert.swift
//  PoP
//
//  Created by XavierTanXY on 9/3/19.
//  Copyright © 2019 SPH. All rights reserved.
//

import Foundation
import UIKit


//A class which handles showing of error
class ErrorAlert {
    
    static let errorAlert = ErrorAlert()
    
    func createAlert(title: String, msg: String, object: UIViewController) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        object.present(alert, animated: true, completion: nil)
    }
    
}
