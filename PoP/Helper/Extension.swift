//
//  Extension.swift
//  PoP
//
//  Created by XavierTanXY on 9/3/19.
//  Copyright © 2019 SPH. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import SDWebImage

//A class for storing extension

extension UIImageView {
    
    //A method where it loads image from an url
    func loadImageUsingCacheWithUrlStringWithoutIndex(url: String) {
        
        self.image = nil
        
        //Running using main queue
        DispatchQueue.main.async() {
            
            //Get image from cache if there is, else download using Alamofire and store in cache
            SDImageCache.shared().queryCacheOperation(forKey: (url as NSString) as String!, done: { (image, data, SDImageCacheType) in
                
                //if there is image in cache
                if( image != nil) {
                    self.image = image
                    
                } else {
                    
                    //Download using Alamofire
                    Alamofire.request(url).responseData { response in
                        
                        if let imageData = response.result.value {
                            let image = UIImage(data: imageData)
                            self.image = image
                            
                            //Store in cache
                            SDImageCache.shared().store(image, forKey: (url as NSString) as String!)
                        } else {
                            
                            //If a track has no image, use default image
                            self.image = UIImage(named: "ProfilePic")
                        }
                    }
                }
            })
            
            
        }

    }
}
