//
//  ViewController.swift
//  PoP
//
//  Created by XavierTanXY on 7/3/19.
//  Copyright © 2019 SPH. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var pickTF: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    var picker: UIPickerView!
    var toolBar: UIToolbar!
    
    var pickerData: [Int] = [Int]()
    var tracks = [Track]()
    
    var current_page = 2
    var per_page: Int?
    var num_pages: Int?
    
    var pagNum = 10
    var tempPaNum = 1
    var usedPeg = false
    
    //Base url for tracks
    var TRACK_URL = "https://api-staging.popsical.tv/v3/songs.json"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.pickTF.delegate = self
        self.picker = UIPickerView()
      
        self.picker.delegate = self
        self.picker.dataSource = self

        loadPicker()
        fetchUrl(url: TRACK_URL)
        
        self.pickerData = [1,2,3,4,5,6,7,8,9,10]
    }
    
    //Setup picker view for pagnination
    func loadPicker() {
        toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: Selector(("donePicker")))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: Selector(("cancelPicker")))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        picker.backgroundColor = UIColor.white
        
        self.pickTF.inputView = picker
        self.pickTF.inputAccessoryView = toolBar
   
    }
    
    @objc func donePicker() {
        usedPeg = true
        self.pagNum = self.tempPaNum
        self.pickTF.resignFirstResponder()
        self.tableView.reloadData()
    }

    
    @objc func cancelPicker() {
        self.tempPaNum = 0
        self.pickTF.resignFirstResponder()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(pickerData[row])"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.tempPaNum = pickerData[row]
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //If custom pagination is used, else use default
        if usedPeg {
            return pagNum
        } else {
            usedPeg = false
            return tracks.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TrackCell") as? TrackCell
        
        cell?.selectionStyle = .none
        cell?.configureCell(track: tracks[indexPath.row])
        cell?.trackImgView!.image = nil
        return cell!
    }
    
    @IBAction func prevTapped(_ sender: Any) {
        
        //Avoid getting page num <= 0
        if (current_page - 1) != 0 {
            current_page = current_page - 1
            fetchUrl(url: "\(TRACK_URL)?page=\(current_page)")
        }
        
    }
    
    @IBAction func nextTapped(_ sender: Any) {
        
        //Avoid getting more than the total pages in database
        if (current_page + 1) != num_pages {
            current_page = current_page + 1
            fetchUrl(url: "\(TRACK_URL)?page=\(current_page)")
        }
    }
    
    @IBAction func pagTapped(_ sender: Any) {
        self.pickTF.becomeFirstResponder()
    }
    
    func fetchUrl(url: String){

        let urlString = URL(string: url)
        
        if let url = urlString {
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
               
                if error != nil {
                 
                    ErrorAlert.errorAlert.createAlert(title: "Error", msg: (error?.localizedDescription)!, object: self)
                } else {
                    
                    if let usableData = data {
                        do {
                            
                            let json = try JSONSerialization.jsonObject(with: usableData, options: []) as! Dictionary<String, AnyObject>

                            if let result = json["tracks"] as? [Dictionary<String, AnyObject>] {

                                self.tracks.removeAll()
                                
                                for track in result {
                                    let t = Track(trackData:track)
                                    
                                    //append all fetched track to array for displaying
                                    self.tracks.append(t)
                    
                                }
                                
                                DispatchQueue.main.async() {
                                    //refresh the view
                                    self.tableView.reloadData()
                                }
                                

                            }
                            
                            if let meta = json["meta"] as? Dictionary<String, AnyObject> {
                                if let totalPage = meta["num_pages"] as? Int {
                                    self.num_pages = totalPage
                                }
                                
                                if let perPage = meta["per_page"] as? Int {
                                    self.per_page = perPage
                                }
                            }
                            
                            
                        } catch let error as NSError {
                   
                            ErrorAlert.errorAlert.createAlert(title: "Error", msg: (error.localizedDescription), object: self)
                        }
                    }
                }
            }
            
            task.resume()
            
        }
    }

}

